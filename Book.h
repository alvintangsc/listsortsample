#pragma once
#include <string>

using namespace std;
class Book
{

public:
	Book(string inTitle, string inAuthor);
	virtual ~Book();
	void printTitle();
	friend ostream& operator<<(ostream &out, Book book);     //output	
	bool operator< (const Book &rhs) const;
	string getAuthor( ) const;
private:
	string title;
	string author;
};

