#include "Book.h"
#include <iostream>
using namespace std;

Book::Book(string inTitle, string inAuthor)
	: title(inTitle), author(inAuthor)
{
}


Book::~Book(void)
{
}

void Book::printTitle()
{
	cout << "Title: " << title << endl;
}


ostream& operator<<(ostream &out, const Book book)     //output
{
	out << "Title: " << book.title << ", " << book.author;
	return out;
}

bool Book::operator< (const Book &rhs) const
{
	return title < rhs.title;
}

string Book::getAuthor( ) const
{
	return author;
}