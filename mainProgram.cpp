#include "Book.h"
#include <iostream>
#include <list>
using namespace std;

bool compare_title (const Book *first, const Book *second)
{
  return *first < *second;
}

bool compare_author (const Book *first, const Book *second)
{
	return first->getAuthor() < second->getAuthor();
}

void main()
{
	list<Book*> library;
	Book* theTemp = new Book("Data Structures and C++", "DS Malik" );
	library.push_back(theTemp);
	library.push_back( new Book( "Lord of the Rings", "Tolkien" ) );
	library.push_back( new Book( "The Little Prince", "Makellivien" ) );
	library.push_back( new Book( "Magician", "Raymond E. Feist" ) );
	library.push_back( new Book( "Life or Death", "Gunner Henry" ) );
	library.push_back( new Book( "Back to the Future", "Marty McFly & Emmett Brown" ) );
	library.push_back( new Book( "My Life", "Bill Clinton" ) );

	list<Book*>::iterator itor = library.begin();
	list<Book*>::iterator end = library.end();

	for( ; itor != end; itor++ )
	{
		cout << **itor << endl;
	}

	cout << "Sorting by author: " << endl;
	//library.sort(); // can't work cos we are using pointers
	//library.sort(compare_title); // sort by title
	library.sort(compare_author); // sort by author
	itor = library.begin();
	end = library.end();

	for( ; itor != end; itor++ )
	{
		cout << **itor << endl;
	}

	cout << "Sorting by title: " << endl;
	//library.sort(); // can't work cos we are using pointers
	library.sort(compare_title); // sort by title
	//library.sort(compare_author); // sort by author
	itor = library.begin();
	end = library.end();

	for( ; itor != end; itor++ )
	{
		cout << **itor << endl;
	}


}